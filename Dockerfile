FROM cgr.dev/chainguard/cc-dynamic 
#gcr.io/distroless/cc-debian12

WORKDIR /app

COPY /target/release/hello_rocket .

ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8000
EXPOSE 8000

CMD ["./hello_rocket"]
